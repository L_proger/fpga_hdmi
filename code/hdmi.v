module hdmi(
	input clk_pixel,
	output [9:0] tmds_r
);

wire clk_pixel_resetn;
wire de, ctrl;

assign clk_pixel_resetn = 1;
assign de = 1;
assign ctrl = 0;

wire [7:0] color_r;

tmds_encoder tmds_encoder_r (
	.clk(clk_pixel),
	.resetn(clk_pixel_resetn),
	.de(de),
	.ctrl(ctrl),
	.din(color_r),
	.dout(tmds_r)
);

endmodule