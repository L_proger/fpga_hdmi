module HDMI_tester(
	input clk_50,
	output TMDS_bh,TMDS_bl,TMDS_gh,TMDS_gl,TMDS_rh,TMDS_rl
);

wire pixclk, clk_TMDS2;
wire [7:0] red, green, blue;

assign red[7:0] = 8'b11110000;
assign green[7:0] = 8'b00001111;
assign blue[7:0] = 8'b00000001;

assign pixclk = clk_50;
assign clk_TMDS2 = clk_50;

wire [10:0] CounterX, CounterY;



HDMI_1280 hdmi(
.pixclk(pixclk), .clk_TMDS2(clk_TMDS2), 
.red(red), .green(green), .blue(blue),
.TMDS_bh(TMDS_bh), .TMDS_bl(TMDS_bl),
.TMDS_gh(TMDS_gh), .TMDS_gl(TMDS_gl),
.TMDS_rh(TMDS_rh), .TMDS_rl(TMDS_rl),
.CounterX(CounterX), .CounterY(CounterY));

endmodule