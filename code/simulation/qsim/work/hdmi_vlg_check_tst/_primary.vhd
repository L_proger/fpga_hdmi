library verilog;
use verilog.vl_types.all;
entity hdmi_vlg_check_tst is
    port(
        \Out\           : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end hdmi_vlg_check_tst;
