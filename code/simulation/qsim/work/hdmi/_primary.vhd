library verilog;
use verilog.vl_types.all;
entity hdmi is
    port(
        \Out\           : out    vl_logic;
        In1             : in     vl_logic;
        In2             : in     vl_logic
    );
end hdmi;
