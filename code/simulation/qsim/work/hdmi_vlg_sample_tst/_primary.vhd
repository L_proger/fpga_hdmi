library verilog;
use verilog.vl_types.all;
entity hdmi_vlg_sample_tst is
    port(
        In1             : in     vl_logic;
        In2             : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end hdmi_vlg_sample_tst;
