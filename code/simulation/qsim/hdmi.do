onerror {quit -f}
vlib work
vlog -work work hdmi.vo
vlog -work work hdmi.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.hdmi_vlg_vec_tst
vcd file -direction hdmi.msim.vcd
vcd add -internal hdmi_vlg_vec_tst/*
vcd add -internal hdmi_vlg_vec_tst/i1/*
add wave /*
run -all
